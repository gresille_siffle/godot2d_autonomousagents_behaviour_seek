## Godot 2D - Autonomous Agents Behaviour - Seek

> An implementation of the steering behaviour "Seek" described by [Craig W. Reynolds] in its document [Steering Behaviors For Autonomous Characters].

[Craig W. Reynolds]: <https://www.red3d.com/cwr/>
[Steering Behaviors For Autonomous Characters]: <https://www.red3d.com/cwr/steer/gdc99/>

This program displays the seek behaviour of several autonomous agents, represented by bird-like objects called boids.

With this individual behaviour the boids seek the mouse cursor in a 2D finite space using a simple algorithm, and
slow down when they approach the target position. Each boid has its own maximum speed, maximum steering force and
mass, and according to these values, moves in a more a less natural way.

For each boid, the UI displays few vectors:

- its current velocity
- the desired velocity to reach the target
- the steering force applied to the current velocity to match desired velocity

The program run on the [Godot] [1] 3.1.1 game engine, and is made with [GDScript] language.

![Simulation Screenshot ](/meta/autonomousagents_behaviour_seek.png)

## Table of Content

- [Install](#Install)
- [Usage](#Usage)
- [License](#License)

## Install

### Dependencies

Download [Godot] [2] 3.1.1 game engine. A self-contained executable of few megabytes, no installation required.

### Open the project

Download the source code and import the `project.godot` file from Godot 3.1.1.

## Usage

... todo ...

## License

[MIT](LICENSE.txt) (c) Gresille&Siffle

[1]: <https://godotengine.org/> "Godot game engine site"
[2]: <https://godotengine.org/download> "Godot game engine download page"
[GDScript]: <http://docs.godotengine.org/en/3.0/getting_started/scripting/gdscript/gdscript_basics.html>
