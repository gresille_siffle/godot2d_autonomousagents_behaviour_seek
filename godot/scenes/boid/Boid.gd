extends Sprite
class_name Boid

export (float) var mass = 4.0
export (int) var max_force = 10  # px/s
export (int) var max_speed = 420 # px/s

onready var visibility_notifier : VisibilityNotifier2D = $VisibilityNotifier2D

var velocity : Vector2 = Vector2()
var acceleration : Vector2 = Vector2()

var steering_force : Vector2 = Vector2()
var desired_velocity : Vector2 = Vector2()

var slowing_distance : int = 128

func _ready():
# warning-ignore:return_value_discarded
    visibility_notifier.connect('screen_exited', self, '_on_VisibilityNotifier_screen_exited')

func seek(target_position : Vector2) -> Vector2:
    """Return the force that needs to be added to the current velocity to seek the target position.

    Use the `max_force` attribute to clamp the force magnitude.
    """
    desired_velocity = (target_position - position).normalized() * max_speed
    steering_force = (desired_velocity - velocity).clamped(max_force)
    return steering_force

func seek_with_mass(target_position : Vector2) ->  Vector2:
    """Return the force that needs to be added to the current velocity to seek the target position.

    Use the `mass` attribute to divided the force magnitude.
    """
    # all intermediate steps are commented to speed up the processing
    #
    desired_velocity = (target_position - position).normalized() * max_speed
    steering_force = ((target_position - position).normalized() * max_speed - velocity) / mass
    return steering_force

func seek_and_arrive(target_position : Vector2) -> Vector2:
    """Return the force that needs to be added to the current velocity to seek and slowly approach the target position.

    Use the `max_force` attribute to clamp the force magnitude. Also divide that magnitude when the target position
    is within the the `slowing_distance` range.
    """
    desired_velocity = (target_position - position)
    var target_distance = desired_velocity.length()
    desired_velocity = desired_velocity.normalized() * max_speed
    desired_velocity = approach_target(slowing_distance, target_distance, desired_velocity)
    steering_force = (desired_velocity - velocity).clamped(max_force)
    return steering_force

# warning-ignore:unused_argument
func approach_target(slowing_distance_ : int, distance_to_target : float, velocity_ : Vector2) -> Vector2:
    if distance_to_target < slowing_distance_:
        velocity_ *= (distance_to_target / slowing_distance_)
    return velocity_

func apply_force(force : Vector2) -> void:
    """Add a force to the current acceleration."""
    acceleration += force

func move(delta : float) -> void:
    """Add the current acceleration to the current velocity, than move the boid."""
    velocity += acceleration
    velocity = velocity.clamped(max_speed)
    rotation = velocity.angle() + PI / 2
    position += velocity * delta
    acceleration = Vector2()

func _on_VisibilityNotifier_screen_exited() -> void:
    var screen_size : Vector2 = get_viewport_rect().size

    if position.x > screen_size.x:
        position.x = 0
    elif position.x < 0:
        position.x = screen_size.x

    if position.y > screen_size.y:
        position.y = 0
    elif position.y < 0:
        position.y = screen_size.y
