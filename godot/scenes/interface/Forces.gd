extends Control

export (NodePath) var boids_path

onready var boids : Node2D

var display_forces : bool = true
var scale_ : float = 0.5

func _ready() -> void:
    boids = get_node(boids_path)

func _draw() -> void:
    for boid in get_node('/root/Simulation/Boids').get_children():
        if display_forces:
            draw_desired_velocity(boid)
            draw_velocity(boid)
            draw_steering_force(boid)

func _input(event : InputEvent) -> void:
    if event.is_action_pressed('ui_toggle_forces'):
        display_forces = not display_forces

# warning-ignore:unused_argument
func _process(delta : float) -> void:
    update()

func draw_velocity(boid : Boid) -> void:
    draw_line(boid.position, boid.position + boid.velocity * scale_, Color(255, 0, 0), 2.0)

func draw_desired_velocity(boid : Boid)  -> void:
    draw_line(boid.position, boid.position + boid.desired_velocity * scale_, Color(255, 255, 0), 2.0)

func draw_steering_force(boid : Boid) -> void:
    draw_line(boid.position, boid.position + boid.steering_force * (1 / scale_), Color('#22b14c'), 2.0)
