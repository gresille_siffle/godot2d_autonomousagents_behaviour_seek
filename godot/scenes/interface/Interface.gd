extends Control

onready var fps : Label = $FPSContainer/HBox/FPS

# warning-ignore:unused_argument
func _process(delta : float) -> void:
    fps.text = str(Engine.get_frames_per_second())

func initiaize() -> void:
    pass
