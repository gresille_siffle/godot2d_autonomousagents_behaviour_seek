extends Node2D

"""
The Simulation
--------------

This node is responsible of running the boids simulation and handling the user interface signals.

Its composed of a `Boids` node, containing all boids. And finally a user interface.
"""

const BOID_SCENE : PackedScene = preload('res://scenes/boid/Boid.tscn')

onready var boids : Node2D = $Boids
onready var interface : Control = $InterfaceLayer/Interface

func _ready() -> void:
    interface.initiaize()

# warning-ignore:unused_argument
func _process(delta : float) -> void:
    """Make the boids move."""

    for boid in boids.get_children():
        boid.apply_force(boid.seek_and_arrive(get_viewport().get_mouse_position()))
        boid.move(delta)
