extends Node2D
class_name Grid_

var _row = 0
var _col = 0
var _grid = []

export (int) var resolution = 100

func _ready() -> void:
    """Create a two-dimensional array to act as a simple spacial database."""
    rebuild(get_viewport_rect().size)

func add(foo : Node2D) -> void:
    """Add a single Node2D to the grid, according to its position.

    Only store a weak reference.
    """
    var x = clamp(int(foo.position.x / resolution), 0, _row - 1)
    var y = clamp(int(foo.position.y / resolution), 0, _col - 1)
    _grid[x][y].append(weakref(foo))

func add_many(foos : Array) -> void:
    """Add several Node2D to the grid, according to their position.

    @see add(foo : Node2D) -> void
    """
    for foo in foos:
        add(foo)

func neighborhood(x : int, y : int) -> Array:
    """Locality query.

    Return all nodes stored in the _grid at (x, y) plus those in the 8 adjacent cells.

               x - 1    x     x + 1
        y - 1 [     ][      ][     ]
          y   [     ][(x, y)][     ]
        y - 1 [     ][      ][     ]

    """
# warning-ignore:narrowing_conversion
    x = clamp(x, 0, _row - 1)
# warning-ignore:narrowing_conversion
    y = clamp(y, 0, _col - 1)
    var neighborhood_ = _grid[x][y]

    if x + 1 < _grid.size():
        neighborhood_ += _grid[x + 1][y]
        if y + 1 < _grid[x].size():
            neighborhood_ += _grid[x + 1][y + 1]
        if y - 1 >= 0:
            neighborhood_ += _grid[x + 1][y - 1]

    if x - 1 >= 0:
        neighborhood_ += _grid[x - 1][y]
        if y + 1 < _grid[x].size():
            neighborhood_ += _grid[x - 1][y + 1]
        if y - 1 >= 0:
            neighborhood_ += _grid[x - 1][y - 1]

    if y + 1 < _grid[x].size():
        neighborhood_ += _grid[x][y + 1]
    if y - 1 >= 0:
        neighborhood_ += _grid[x][y - 1]

    return neighborhood_

func clear() -> void:
    """Clear the _grid content, keeping the size of its dimensions."""
    for x in range(_row):
        for y in range(_col):
            _grid[x][y].clear()

func rebuild(screen_size : Vector2) -> void:
    """Rebuild the _grid according to the given screen size.

    Delete all of its content.
    """
    _grid.clear()
    _row = int(screen_size.x / resolution)
    _col = int(screen_size.y / resolution)
    for x in range(_row):
        _grid.append([])
# warning-ignore:unused_variable
        for y in range (_col):
            _grid[x].append([])
